<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Location;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->delete();

        
        Location::create([
            'name'=> 'Seaweed Bay',
            'address'=> '9349 Thomas Street',
            'category'=> '1',
            'latitude'=>'51.50217920','longitude'=>'-0.11545944',
        ]);
        Location::create([
            'name'=> 'Peril Island',
            'address'=> 'Evanston, IL 60201',
            'category'=> '2',
            'latitude'=>'51.51124458','longitude'=>'-0.08535004',
        ]);
        Location::create([
            'name'=> 'Broken Tooth Enclave',
            'address'=> '7 Nicolls St.',
            'category'=> '3',
            'latitude'=>'51.51894780','longitude'=>'-0.10481644',
        ]);
        Location::create([
            'name'=> 'Crows Nest Atoll',
            'address'=> 'Wenatchee, WA 98801',
            'category'=> '4',
            'latitude'=>'51.50175135','longitude'=>'-0.09503174',
        ]);
        Location::create([
            'name'=> 'Scurvy Cavern',
            'address'=> '18 Manhattan Dr.',
            'category'=> '5',
            'latitude'=>'51.49650985','longitude'=>'-0.08215714',
        ]);
        Location::create([
            'name'=> 'Sanctuary of the Tempest',
            'address'=> 'Massapequa Park, NY 11762',
            'category'=> '6',
            'latitude'=>'51.49158871','longitude'=>'-0.10739136',
        ]);
        Location::create([
            'name'=> 'Cavern of Timber',
            'address'=> '206 Argyle Lane',
            'category'=> '1',
            'latitude'=> '53.60541267',
            'longitude'=> '-0.02219888',
        ]);
        Location::create([
            'name'=> 'Sanctuary of Gunpowder',
            'address'=> 'Winston Salem, NC 27103',
            'category'=> '2',
            'latitude'=>'51.49548287','longitude'=>'-0.13166428',
        ]);
        Location::create([
            'name'=> 'Haven of the Unknown',
            'address'=> '8203 Constitution Drive',
            'category'=> '3',
            'latitude'=>'51.50538795','longitude'=>'-0.10069656',
        ]);
        Location::create([
            'name'=> 'Anchorage of Macaws',
            'address'=> 'Hempstead, NY 11550',
            'category'=> '4',
            'latitude'=>'51.49725867','longitude'=>'-0.10704804',
        ]);
        Location::create([
            'name'=> 'New Sense Bank Group',
            'address'=> '7 Edgewood Street',
            'category'=> '5',
            'latitude'=>'51.49715170','longitude'=>'-0.07031250',
        ]);
        Location::create([
            'name'=> 'United Bank Inc.',
            'address'=> 'Shelbyville, TN 37160',
            'category'=> '6',
            'latitude'=>'51.50806174','longitude'=>'-0.06035614',
        ]);
        Location::create([
            'name'=> 'Elite Bancorp',
            'address'=> '7100 Annadale Court',
            'category'=> '1',
            'latitude'=>'51.50003990','longitude'=>'-0.04748154',
        ]);
        Location::create([
            'name'=> 'Life Tree Financial Services',
            'address'=> 'Westfield, MA 01085',
            'category'=> '2',
            'latitude'=>'51.51501286','longitude'=>'-0.07735062',
        ]);
        Location::create([
            'name'=> 'Pillar Bank Inc.',
            'address'=> '130 Thomas St.',
            'category'=> '3',
            'latitude'=>'51.51918302','longitude'=>'-0.05194473',
        ]);
        Location::create([
            'name'=> 'Nation Holding Company',
            'address'=> 'Reisterstown, MD 21136',
            'category'=> '4',
            'latitude'=>'51.50378361','longitude'=>'-0.08387375',
        ]);
        Location::create([
            'name'=> 'Premium Bancshares',
            'address'=> '363 Pendergast Ave.',
            'category'=> '5',
            'latitude'=>'51.49854234','longitude'=>'-0.08713532',
        ]);
        Location::create([
            'name'=> 'Better Life Financial',
            'address'=> 'Solon, OH 44139',
            'category'=> '6',
            'latitude'=>'51.49107242','longitude'=>'-0.07007217',
        ]);
        Location::create([
            'name'=> 'Gold Alliance Bank Group',
            'address'=> '6 Lower River Dr.',
            'category'=> '1',
            'latitude'=>'51.51618910','longitude'=>'-0.13348389',
        ]);
        Location::create([
            'name'=> 'Principal Holdings',
            'address'=> 'Lake Mary, FL 32746',
            'category'=> '2',
            'latitude'=>'51.51298111','longitude'=>'-0.12112427',
        ]);
        Location::create([
            'name'=> 'Flair Play',
            'address'=> '51 NE. Sleepy Hollow Road',
            'category'=> '3',
            'latitude'=> '50.49109195',
            'longitude'=> '-0.04990919',
        ]);
        Location::create([
            'name'=> 'Giftsakes',
            'address'=> 'Wethersfield, CT 06109',
            'category'=> '4',
            'latitude'=>'51.50763395','longitude'=>'-0.14927673',
        ]);
        Location::create([
            'name'=> 'Sew Crafty',
            'address'=> '9276 Kent Street',
            'category'=> '5',
            'latitude'=>'51.48955591','longitude'=>'-0.09056854',
        ]);
        Location::create([
            'name'=> 'Piece of My Art',
            'address'=> 'Zion, IL 60099',
            'category'=> '6',
            'latitude'=>'51.51747224','longitude'=>'-0.04027176',
        ]);
        Location::create([
            'name'=> 'Flair is Fair',
            'address'=> '7841 Mechanic Ave.',
            'category'=> '1',
            'latitude'=>'51.50078867','longitude'=>'-0.08765030',
        ]);
        Location::create([
            'name'=> 'And Sew On',
            'address'=> 'Lagrange, GA 30240',
            'category'=> '2',
            'latitude'=>'51.52046607','longitude'=>'-0.09434509',
        ]);
        Location::create([
            'name'=> 'Apothecrafty',
            'address'=> '662 S. Vine St.',
            'category'=> '3',
            'latitude'=>'51.50742005','longitude'=>'-0.05074310',
        ]);
        Location::create([
            'name'=> 'In These Arts',
            'address'=> '9580 Fairground Rd.',
            'category'=> '4',
            'latitude'=>'51.50132349','longitude'=>'-0.03958511',
        ]);
        Location::create([
            'name'=> 'Seam On',
            'address'=> 'Leominster, MA 01453',
            'category'=> '5',
            'latitude'=>'51.49340745','longitude'=>'-0.02121735',
        ]);
        Location::create([
            'name'=> 'Spun Fun',
            'address'=> '74 Harrison Dr.',
            'category'=> '6',
            'latitude'=>'51.51201866','longitude'=>'-0.05108643',
        ]);
        Location::create([
            'name'=> 'Foolplay Arcade',
            'address'=> 'Dublin, GA 31021',
            'category'=> '1',
            'latitude'=>'51.51330192','longitude'=>'-0.06396103',
        ]);

    }
}
