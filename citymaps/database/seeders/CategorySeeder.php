<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        Category::create(['category' => 'Alimentação', 'color' => '#fff714']);
        Category::create(['category' => 'Tecnologia', 'color' => '#e00791']);
        Category::create(['category' => 'Serviços', 'color' => '#479400']);
        Category::create(['category' => 'Distribuição', 'color' => '#e07407']);
        Category::create(['category' => 'Saúde', 'color' => '#006eff']);
        Category::create(['category' => 'Outro', 'color' => '#8132a8']); 
        
    }
}
