<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call(CategorySeeder::class);

        $this->command->info('Category table seeded!');
        
        $this->call(LocationSeeder::class);

        $this->command->info('Location table seeded!');
    }

}


