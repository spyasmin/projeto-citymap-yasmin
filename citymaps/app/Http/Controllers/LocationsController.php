<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\Category;


class LocationsController extends Controller
{
    public function welcome(){

        $categories = Category::all();
        
        $locations = Location::all()->map(function ($location, $key) {
            return [
                //'latitude' => $location->latitude,
                //'longitude' => $location->longitude,
                'latlng' => [$location->latitude, $location->longitude],
                'name' => $location->name,
                'address' => $location->address,
                'category'=> $location->category,
                'created_at' => $location->created_at,
            ];
        })->values();

        return view('welcome', compact('locations', 'categories'));
    }

    public function store(Request $request){
        
        $request->validate([
            'name' => 'required',
            'address' => 'required'
        ]);
        

        $location = Location::create($request->all());
        
        return redirect()->route('welcome')
            ->with('success', 'Localização criada com sucesso.');

    }
}
