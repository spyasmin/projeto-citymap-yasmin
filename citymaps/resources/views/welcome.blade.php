<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Projeto CityMap</title>
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin=""/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        
        
    </head>
    
    <body > 
        <main class="d-flex flex-column h-100">
            <header class="site-header">
                <p href="#" class="brand">Projeto CityMap</p>

            </header>
            
            <div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </div>
            
            <section>
                <div class="container" id="mapid">
                </div>
            </section>
                    
            <footer class="footer py-3 ">
                <div class="container">
                    <p>Avaliação Geoinova para vaga perfil jr.</p>
                    <p>Yasmin Abreu Soares de Souza Pimentel</p>
                    <p>spyasmin@gmail.com</p>
                    <p>+55 61 98221 3761 </p>
                </div>
            </footer>
        </main>

        <div class="modal" tabindex="-1" id="modal" >
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Adicionar novo ponto de interesse</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Atenção!</strong> Ocorreram erros durante operação.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <form role="form" action="{{ route('register_location') }}" method="POST">
                @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Nome</label>
                            <div>
                                <input type="text" class="form-control input-lg" name="name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Endereço</label>
                            <div>
                                <input type="text" class="form-control input-lg" name="address" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="category">Categoria:</label>
                            <select class="form-select" name="category" id="category" >
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" class="form-control" id="latitude" name="latitude" >
                        <input type="hidden" class="form-control" id="longitude" name="longitude" >
                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
        

        <!-- Javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
         integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
         crossorigin=""></script>

        <script>       
            
            $(document).ready(function(){

                //Variaveis
                var mymap = L.map('mapid').setView([51.505, -0.09], 13);
                var locations = {!! json_encode($locations) !!};
                var categories = {!! json_encode($categories) !!};
                var card_text = "";


                //gera texto da legenda conforme tabela de categorias
                categories.forEach(function(category) {
                    card_text = card_text.concat("<li class='list-group-item'><span class='badge badge-pill' style='background-color:", category.color, ";color:",category.color,"'>></span>",category.category,"</li>");
                });

                //mapa

                L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic3B5YXNtaW4iLCJhIjoiY2tvOWgzN3MzMmhvbDJ3bnNoYnpocHpyMSJ9.SQgWNaoyQCS7P0J2w7D-gA', {
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: 'mapbox/streets-v11',
                    tileSize: 512,
                    zoomOffset: -1
                }).addTo(mymap);    
                
                
                //povoa mapa + adiciona popups --> conforme documentação do leaflet
                locations.forEach(function(location) {
                    var marker = L.circle(location.latlng, {
                        color: categories.find(x => x.id === (location.category)).color,
                        fillColor: categories.find(x => x.id === (location.category)).color,
                        fillOpacity: 0.8,
                        radius: 30
                    }).addTo(mymap);
                    marker.bindPopup("<b>"+location.name +"</b><br>" + location.address);
                    marker.on('mouseover', function (e) {
                        this.openPopup();
                    });
                    marker.on('mouseout', function (e) {
                        this.closePopup();
                    });
                });

                //cria legenda no canto superior direito --> feito conforme documentação do leaflet e com elementos de bootstrap
                L.Control.textbox = L.Control.extend({
                    onAdd: function(map) {
                        
                    var card = L.DomUtil.create('div');
                    card.id = "info_text";
                    card.innerHTML = "<div class='card'><div class='card-body'><ul class='list-group list-group-flush'>"+card_text+"</ul></div></div>";
                    return card;
                    },

                    onRemove: function(map) {
                    }
                });
                L.control.textbox = function(opts) { return new L.Control.textbox(opts);}
                L.control.textbox({ position: 'topright' }).addTo(mymap);


                //chama modal de cadastro de novo ponto de interesse
                mymap.on('click', function(e) {
                    $('#modal').modal('show');
                    $("#latitude").val(e.latlng.lat);
                    $("#longitude").val(e.latlng.lng);
                });


            });  

            </script>
    </body>

</html>
    