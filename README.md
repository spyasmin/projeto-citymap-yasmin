# CityMap

Essa é a execução do projeto teste da empresa. Foi desenvolvido em PHP, utilizando o framework Laravel 8 e a biblioteca Leaflet.
O ambiente de desenvolvimento foi o Visual Studio Code + XAMP

## Instalação

Uma vez clonado o projeto 

    - Alterar os parametros do .env para conectar à sua base de dados.
    - Executar os comandos de seed para o banco de dados

```bash
php artisan migrate
php artisan db:seed
```

E então

```bash
php artisan serve
```

## Sobre

O projeto não saiu muito do escopo do que foi solicitado no documento. A opção por não utilizar o PostgreSQL
se deu por questão de velocidade de produção, optando pelo ambiente que já estava estabelecido no computador.



